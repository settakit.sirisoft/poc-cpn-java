FROM adoptopenjdk/openjdk11:centos

ARG UNAME=cpn

ARG GNAME=cpn

ARG UID=2000

ARG GID=2000

ARG JAR_FILE=target/*.jar

# ARG APP_PROFILES_ACTIVE=cpnuat

USER root

RUN mkdir /app

RUN groupadd -g ${GID} ${GNAME}

RUN useradd -m -u ${UID} -g ${GID} -s /bin/bash ${UNAME} -d /app/cpn

RUN mkdir -p /app/cpn

RUN echo "securerandom.source=file:/dev/urandom" >> /opt/java/openjdk/conf/security/java.security

ENV TZ Asia/Bangkok

# ENV SPRING_PROFILES_ACTIVE=${APP_PROFILES_ACTIVE}

COPY ${JAR_FILE} /app/cpn/airpay-service.jar

USER ${UNAME}

WORKDIR /app/cpn

EXPOSE 8081

CMD ["java","-Dserver.port=8080","-jar","/app/cpn/airpay-service.jar"]